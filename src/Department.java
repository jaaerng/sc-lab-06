
public class Department {
	
	private String department;
	
	public Department(String departmentName) {
		this.department = departmentName;
	}

	public String getName() {
		return department;
	}

	public void setName(String name) {
		this.department = name;
	}
	
	public String toString() {
		return "Department: " + getName();
	}
	
}
