import java.util.ArrayList;


public class Teacher extends Person {

	private ArrayList<String> courses = new ArrayList<String>();   
	private String faculty;

	public Teacher(String name, String faculty) {
		super(name);
		this.setFaculty(faculty);
	}

	public boolean addCourse(String course) {
		for (String s:courses) {
			if (s.equals(course)) {
				return false;
			}
		}
		courses.add(course);
		return true;
	}
	
	public String printCourse() {
		String course = "";
		for (String c:courses) {
			course += c;
		}
		return course;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}
	
	@Override
	public String toString() {
		return "Teacher: " + super.getName() + "\tFaculty: " + getFaculty() + "\n\tCourse\n" + printCourse();
	}

}
