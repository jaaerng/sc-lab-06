
public class Main {

	public static void main(String[] args) {
		Student student = new Student("Un", "24/07");
		Teacher teacher = new Teacher("Thun", "Science");
		
		ComputerScience cs = new ComputerScience("Computer Science", "Science");
		
		student.addCourseGrade("Software Construction", 4);
		teacher.addCourse("Software Construction");
		
		cs.addMember(student);
		cs.addMember(teacher);
		
		System.out.println(student.toString());
		System.out.println("--------------------------------------");
		System.out.println(teacher.toString());
		System.out.println("--------------------------------------");
		System.out.println(cs.toString());
	}

}
