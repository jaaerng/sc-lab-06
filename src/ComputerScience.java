import java.util.ArrayList;


public class ComputerScience extends Department {

	private ArrayList<Person> personList = new ArrayList<Person>();
	private String faculty;
	private int numMembers;
	
	public ComputerScience(String deparmentName, String facultyname) {
		super(deparmentName);
		this.setFaculty(facultyname);
	}
	
	public void addMember(Person person) {
		personList.add(person);
		setNumMembers(getNumMembers() + 1);
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public int getNumMembers() {
		return numMembers;
	}

	public void setNumMembers(int numMembers) {
		this.numMembers = numMembers;
	}
	
	@Override
	public String toString() {
		return super.toString() + "\nNumber of Members: " + getNumMembers();
	}
}
