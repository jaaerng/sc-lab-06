
public class Student extends Person{

	private String address;
	private int numCourses;    
	private String[] courses;
	private int[] grades;    
	private static final int MAX_COURSES = 30;

	public Student(String name, String address) {
		super(name);
		this.numCourses = 0;
		this.courses = new String[MAX_COURSES];
		this.grades = new int[MAX_COURSES];
		this.setAddress(address);
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void addCourseGrade(String course, int grade) {
		courses[numCourses] = course;
		grades[numCourses] = grade;
		++numCourses;
	}

	public double getAverageGrade() {
		int sum = 0;
		for (int i = 0; i < numCourses; i++ ) {
			sum += grades[i];
		}
		return (double)sum/numCourses;
	}
	
	public String printCourseGrade() {
		String courses = "";
		for (int i = 0; i < numCourses; i++ ) {
			courses += this.courses[i] + "\t\t" + grades[i];
		}
		return courses;
	}

	@Override
	public String toString() {
		return "Student: " + super.getName()+ "\tAddress: " + getAddress() + "\n\tCourse\t\t\tGrade\n" + printCourseGrade();
	}
}
